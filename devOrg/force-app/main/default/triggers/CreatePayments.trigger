trigger CreatePayments on vlocity_ps__Case__c (after insert) {
    System.debug ('Trigger start');

    List<Benefit_Payment__c> payments = new List<Benefit_Payment__c>();

    Integer currentMonth = Date.today().month();
    Integer currentYear = Date.today().year();
    
    for( vlocity_ps__Case__c bc : Trigger.new) // this bulkifies the trigger
        if (bc.vlocity_ps__Type__c == 'Food Assistance') { // This trigger only works for SNAP

        System.debug (bc.vlocity_ps__Type__c);

        for (Integer monthCounter = 1; monthCounter < 12; monthCounter++) { // loop through 12 times
            Benefit_Payment__c p = new Benefit_Payment__c();
            p.Benefit_Case__c = bc.Id;
            p.Amount__c = bc.Monthly_Benefit__c;
            p.Program__c = 'SNAP';
            p.Type__c = 'Disbursement';
            p.Status__c = 'Pending';
            p.Payment_Method__c = 'DebitCard';
            p.Payee__c = bc.vlocity_ps__PrimaryContactId__c;
            p.Payment_Frequency__c = 'Monthly';
            
            // Create ERP voucher
            Double random =  Math.random();
            random = random * 10000;
            Integer voucher_number = random.intValue();
            String voucher_number_string = string.valueof(voucher_number);
            p.Voucher_Id__c = 'ERP-' + voucher_number_string;

            // calculate payment date
            Integer paymentMonth;
            Integer paymentYear;
            
            paymentMonth = currentMonth + monthCounter;
            paymentYear = currentYear;
            
            if (paymentMonth > 12) {  // account for the cycling of the calendar
                paymentMonth = paymentMonth - 12;
                paymentYear++;
            }

            p.Payment_Date__c = date.newInstance(paymentYear, paymentMonth, 1);

            // add payment to the list
            payments.add (p);
            }
            }
        
     try {
            insert payments; // insert all of the payments at once
        } catch (system.Dmlexception e) {
            system.debug (e);
        }

}