trigger CaseTrigger on Case (before insert, before update) {
    
    if(Trigger.isBefore){
        Database.DMLOptions dmlOpts = new Database.DMLOptions(); 
        dmlOpts.assignmentRuleHeader.useDefaultRule= true;    
        
        for(Case c : Trigger.new){
            c.setOptions(dmlOpts); 
        }
    }
}