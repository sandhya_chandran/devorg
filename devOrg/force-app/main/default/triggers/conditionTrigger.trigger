trigger conditionTrigger on Condition__c (after insert,after update) {

    if(Trigger.isAfter){
       List<Contact> cons = new List<Contact>();
       Contact con;
       for(Condition__c c : Trigger.new){
          con = new Contact();
          con.id = c.contact__c;
          con.Total_Score__c = c.Total_Score__c;
          cons.add(con);
       }
       
       if(cons.size() > 0){
           update cons;
       }
       
       
    }

}