({
    fetchFile: function(component, event, helper){
        // 'url' : '/services/data/v50.0/sobjects/ContentVersion/0685w00000FwaWrAAJ/VersionData',
        // 'url' : '/services/data/v50.0/sobjects/ContentVersion/0685w00000FwaWrAAJ/VersionData',
        component.find( 'lc_api' ).restRequest({
            'url' : '/services/data/v50.0/sobjects/ContentVersion/0685w00000FxBd0AAF/VersionData',
            'method' : 'get'
        }).then( $A.getCallback( function( response) {
           // var blob = new Blob([response], {type: "octet/stream"});
            console.log('=====response======');
            //console.log(blob);
            
            var zipFile = new JSZip();
            zipFile.file('test.pdf', response, {base64 : true});
            console.log('==== create the zip =====');
            zipFile.generateAsync({
                type: 'blob'
            }).then(function(content){
                
                saveAs(content, "b.zip");
            });
            
        })).catch( $A.getCallback( function( err ) {
            // handle error
        }));    
    },
        fetchFile2: function(component, event, helper){
        
        component.find( 'lc_api' ).fetchRequest ({
            'url' : 'https://csa-covid19.my.salesforce.com/sfc/servlet.shepherd/version/download/0685w00000Fu4zaAAB'
        }).then( $A.getCallback( function( response) {
            
            var zipFile = new JSZip();
            zipFile.file('test.png', response);
            zipFile.generateAsync({
                type: 'blob',
                streamFiles: true
            }).then(function(content){
                
                saveAs(content, "a.zip");
            });
            
        })).catch( $A.getCallback( function( err ) {
            // handle error
        }));    
    },
    
    onPress : function(component, event, helper) {
        console.log('=== onPress ===');
        //var fileIds = ['0691I00000IK4f2QAD','0691I00000IK4f3QAD'];
        //let fileIds = ['0695w00000F4mkGAAR','0695w00000A5q7KAAR','0695w00000FBU7LAAX'];
        //let fileIds = ['0695w00000F4mkGAAR','0695w00000A5q7KAAR','0695w00000FBTgVAAX'];
        let fileIds = ['0695w00000FBjmRAAT','0695w00000FBjssAAD']; //18M, 19M
        //let fileIds = ['0695w00000FBjrGAAT'];//20M
        //let fileIds = ['0695w00000FBk29AAD'];//21M
        //let fileIds = ['0695w00000FBk2AAAT'];//22M
        //let fileIds = ['0695w00000F4mUlAAJ']; //21.9M
        let zipFile = new JSZip();
        let promiseActions = [];
        
        fileIds.forEach( fileId => {
            promiseActions.push(helper.fetchFileUsingPromise(component, fileId));         
        });
            
            Promise.all(promiseActions)
            .then($A.getCallback(function(results){
            let size = fileIds.length;
            //var zipFile = new JSZip();
            results.forEach(result => {
            zipFile.file(result.fileName + '.' + result.fileExtension, helper.base64ToBlob(result.content));
        });
            
            zipFile.generateAsync({type:"blob"}).then(function(content){
            
            console.log('====content=====');
            
            saveAs(content, "zipfiles.zip");
        });
        
        
    })) 
    .catch($A.getCallback(function () { 
    console.log('Some error has occured'); 
}));


/*
        var packageXml = '<?xml version="1.0" encoding="UTF-8"?>' + 
            '<Package xmlns="http://soap.sforce.com/2006/04/metadata">' +
            '<version>35.0</version>' +
            '</Package>' ;
        
        var zipFile = new JSZip();
        console.log('===zipFile===' + JSON.stringify(zipFile));        
        zipFile.file('package.xml',packageXml);
        console.log('===zipFile2===' + JSON.stringify(zipFile));  
        var data = zipFile.generateAsync({type:"blob"}).then(function(d){
            
            console.log('====d=====');
            
            console.log(JSON.stringify(d));
            saveAs(d, "example.zip");
        });
        */
/*
        var zip = new JSZip();
        zip.file("Hello.txt", "Hello World\n");
        //var img = zip.folder("images");
        //img.file("smile.gif", imgData, {base64: true});
        zip.generateAsync({type:"blob"})
        .then(function(content) {
            // see FileSaver.js
            saveAs(content, "example.zip");
        });
        */


},
    
    downloadFile: function(component, event, helper){
        console.log('download');
        var url = 'https://csa-covid19.my.salesforce.com/sfc/servlet.shepherd/version/download/0685w00000Fu4zaAAB';
        url = 'https://csa-covid19.lightning.force.com/sfc/servlet.shepherd/version/download/0685w00000Fu4zaAAB';
        // var url = 'https://osb-impact-integration-s-osb-impact-integration-dev.apps.dev.ocp-dev.ised-isde.canada.ca/getFileFromSalesforce/0694c0000002uXfAAI';
        //url = 'https://upload.wikimedia.org/wikipedia/commons/c/c5/Skegness_Clock_Tower_-_geograph.org.uk_-_1490888.jpg';
        url = 'https://csa-covid19.my.salesforce.com/sfc/p/5w000002sDpK/a/5w000000gOI1/A6k48Ktw2kQUAa2gpXw4HUNIS6C1MolQyEX6i0tb7ZM';
        fetch(url, {
            method: 'GET',
            mode: 'cors',
            credentials: 'same-origin'
            /*,
                        headers: {
                            'Authorization': token
                        }*/
        }).then(function(resp) {
            console.group('=====resp=====');
            console.group(resp);
            console.groupEnd();
            console.group('=====resp blob=====');
            console.groupEnd();
            return resp.blob();
        }).then(function(blob) {
            console.group('=====zip=====');
            console.log(blob);
            console.groupEnd();
            var zipFile = new JSZip();
            zipFile.file('tst.exe', blob);
            zipFile.generateAsync({type:"blob"}).then(function(content){                        
                
                saveAs(content, "tst.zip");
            });
        });
    }
})