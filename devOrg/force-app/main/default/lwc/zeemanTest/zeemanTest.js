import { LightningElement, track, wire } from "lwc";
import getBookingTypes from "@salesforce/apex/BookingService.getBookingTypes";
import {
  getBookingSlotDetails,
  updateBookingSlotContact
} from "c/bookingService";

export default class ZeemanTest extends LightningElement {}