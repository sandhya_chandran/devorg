import { LightningElement, api, wire} from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getOfficeBuildings from '@salesforce/apex/Back2WorkController.getOfficeBuildings';
import getavailableFloorsAndWorkspaces from '@salesforce/apex/Back2WorkController.getavailableFloorsAndWorkspaces';
import createBooking from '@salesforce/apex/Back2WorkController.createBooking';

//For getting the current user's information
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import Id from '@salesforce/user/Id';
import EMAIL_FIELD from '@salesforce/schema/User.Email';
import CONTACT_FIELD from '@salesforce/schema/User.ContactId';
const fields = [EMAIL_FIELD, CONTACT_FIELD];
const TEXT_SIZE_DOC = "slds-text-heading_medium";

export default class BackToWorkScheduler extends LightningElement {
    //For getting the current user's information
    userId = Id;
    @api defaultContactId = '0035w00003JRoawAAD';
    @api contactId;
    @wire(getRecord, { recordId: '$userId', fields }) user;

    //Padding, Classes, Sizes and Styles for the doc
    @api input_small_size = 12;
    @api label_normal_size = 6;
    @api input_normal_size = 6;
    @api text_size = TEXT_SIZE_DOC;
    @api label_class = "slds-form-element__label " + TEXT_SIZE_DOC;
    @api lightning_item_padding = "around-medium";
    @api button_class = "slds-button slds-button_brand " + TEXT_SIZE_DOC;
    @api button_stretch_class = "slds-button slds-button_brand slds-button_stretch " + TEXT_SIZE_DOC;
    @api table_class = "slds-p-around_medium " + TEXT_SIZE_DOC;

    @api officeId;
    @api officeName;
    @api officeBuildings = [];
    @api sDate = new Date();
    @api eDate = new Date();
    @api dd = (new Date()).toISOString().slice(0,10);
    @api s = "09:00:00.000";
    @api e = "17:00:00.000";
    @api s_label;
    @api e_label;
    //movePrev - 1st screen. moveNext - 2nd screen. confirmScreen - 3rd Screen
    @api movePrev = false;
    @api moveNext = false;
    @api confirmScreen = false;
    //This checks to see whether user has clicked the FINISH button multiple times
    @api alreadyBooked = false;

    @wire(getOfficeBuildings)
    wiredGetOfficeBuildings({ error, data }) {
        if (data) {
            this.officeBuildings = data.map(function (o){ return {label:o.Name, value:o.Id}});
            this.error = undefined;
        } else if (error) {
            this.error = error;
            console.log(error);
            this.officeBuildings = undefined;
        }
    };   

    officeBuildingChange(event) { 
        this.officeId = event.target.value;
        this.officeName = event.target.options.find(opt => opt.value === event.detail.value).label; 
    }
    schedDateChange(event) { this.dd = event.target.value; }
    sDateChange(event){ this.s = event.target.value; }
    eDateChange(event){ this.e = event.target.value; }

    @api availableFloors;
    @api floorId;
    @api floorName;
    @api mapOfAvailableOfWorkspaces;
    @api availableWorkspaces;
    @api workspaceId;
    @api workspaceName;
    @api returnType;
    errorToast(t, m){
        const evt = new ShowToastEvent({ title: t, message: m, variant: 'error' });
        this.dispatchEvent(evt);  
    }
    successToast(t,m){
        const evt = new ShowToastEvent({ title: t, message: m, variant: 'success' });
        this.dispatchEvent(evt);
    }
    checkNext(event) {
        //First we define the values we will be submitting to the apex method
        let gmtStr = this.sDate.toString().slice(25,33);
        this.sDate = new Date(Date.parse(this.dd + ' ' + this.s + ' ' + gmtStr));
        this.eDate = new Date(Date.parse(this.dd + ' ' + this.e + ' ' + gmtStr));
        //In reality should use the line below, but is not in community so will do this way:
        this.contactId = getFieldValue(this.user.data, CONTACT_FIELD);
        if (!this.contactId) { this.contactId = this.defaultContactId; }
        //console.log(gmtStr + '\n' + this.sDate + '\n' + this.eDate + '\n' + this.contactId);
        //console.log([this.sDate.getFullYear(), (this.sDate.getMonth()+1), this.sDate.getDate(),this.sDate.getHours(), this.sDate.getMinutes(), this.eDate.getHours(), this.eDate.getMinutes()]);
        
        //Cleans up s and e to only be HH:MM
        this.s_label = this.s.slice(0,5);
        this.e_label = this.e.slice(0,5);
        //Now we ensure that nothing incorrect was submitted
        if (!this.officeId){ //Checks that an office building has been selected
            this.errorToast('NO OFFICE BUILDING SELECTED',
            'You have not selected any office building. Please select one of them before pressing NEXT');
        }
        else if (this.sDate > this.eDate){ //ensures that startDate is before the endDate
            this.errorToast('INVALID TIME SLOT SELECTION',
            'You have selected a start time that is later than the end time you selected for this appointment. ' +
            'Please fix this before clicking NEXT again');
        } else {
            //Makes sure to clear any values from before
            this.availableFloors = [];
            this.floorId = '';
            this.mapOfAvailableOfWorkspaces = [];
            this.availableWorkspaces = [];
            this.workspaceId = '';
            getavailableFloorsAndWorkspaces({officeId:this.officeId,
                                            contactId:this.contactId, 
                                            dYear:this.sDate.getFullYear(), 
                                            dMonth:(this.sDate.getMonth()+1), 
                                            dDay:this.sDate.getDate(),
                                            sHour:this.sDate.getHours(), 
                                            sMinute:this.sDate.getMinutes(), 
                                            eHour:this.eDate.getHours(), 
                                            eMinute:this.eDate.getMinutes()}).then(result=>{
                let res = JSON.parse(result);
                this.returnType = res.returnType;
                if (this.returnType == 0){
                    this.errorToast('CANNOT BOOK APPOINTMENT',
                    'The time slot you have selected conflicts with one that you have already selected previously.' +
                    ' Please choose a different time period before clicking NEXT');
                } else if (this.returnType == 1){
                    this.errorToast('NO WORKSPACE AVAILABLE',
                    'There do not seem to be any available workspaces for the building and time period you have' +
                    ' selected. Please either select another building or another time slot before clicking NEXT');
                } else {
                    this.availableFloors = res.availableFloors.map(function (o){ return {label:o.CSA_Floor_Number__c, value:o.Id}});
                    this.mapOfAvailableOfWorkspaces = res.mapOfAvailableOfWorkspaces;
                    this.movePrev = true;
                    this.moveNext = true;
                    this.alreadyBooked = false;
                }
            }).catch(error=>{
                this.errorToast('System Error',
                error);
                console.log(error);
            })
        }
    }

    floorChanged(event) {
        this.floorId = event.target.value;
        this.availableWorkspaces = this.mapOfAvailableOfWorkspaces[this.floorId];
        this.floorName = event.target.options.find(opt => opt.value === event.detail.value).label;
    }

    bookOnThisDesk(event){
        this.workspaceId = event.target.name;
        let ws = this.availableWorkspaces.find( ({ Id }) => Id === this.workspaceId );
        this.workspaceName = ws.CSA_Workspace_Name__c;
        this.moveNext = false;
        this.confirmScreen = true;
    }

    checkPrev(event){
        this.movePrev = false;
        this.moveNext = false;
        this.confirmScreen = false;
    }

    checkPrev2(event){
        this.moveNext = true;
        this.confirmScreen = false;
    }

    clearVariables(){
        this.officeId = '';
        this.sDate = new Date();
        this.eDate = new Date();
        this.dd = (new Date()).toISOString().slice(0,10);
        this.s = "09:00:00.000";
        this.e = "17:00:00.000";
        this.movePrev = false;
        this.moveNext = false;
        this.confirmScreen = false;
        this.availableFloors =[];
        this.floorId = '';
        this.mapOfAvailableOfWorkspaces = [];
        this.availableWorkspaces = [];
        this.workspaceId = '';
        this.workspaceName = '';
        this.returnType = 0;
    }
    
    finishBooking(event){
        if (!this.alreadyBooked){
            this.alreadyBooked = true;
            console.log(this.alreadyBooked);
            createBooking({contactId:this.contactId, 
                workspaceId:this.workspaceId, 
                dYear:this.sDate.getFullYear(), 
                dMonth:(this.sDate.getMonth()+1), 
                dDay:this.sDate.getDate(),
                sHour:this.sDate.getHours(), 
                sMinute:this.sDate.getMinutes(), 
                eHour:this.eDate.getHours(), 
                eMinute:this.eDate.getMinutes()}).then(result=>{
                    if (result){
                        this.successToast('APPOINTMENT BOOKED',
                        'Your appointment has been successfully booked. You should receive an email in a short while.');
                        this.clearVariables();
                    } else {
                        this.errorToast('ERROR ON SAVE',
                        'An error occured on trying to save your appointment record. Please try again at a later time.');
                        this.alreadyBooked = false;
                    }
                });
        } else {
            console.log("You have clicked FINISH multiple times. Please do not do this in the future");
        }
    }
}