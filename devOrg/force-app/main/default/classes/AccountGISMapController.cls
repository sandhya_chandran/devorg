public with sharing class AccountGISMapController {
    
    
    public static map<string,GIS_Map_Layer__mdt> getLayerSettings() {
        map<string,GIS_Map_Layer__mdt> layerSettingMap=new map<string,GIS_Map_Layer__mdt>();
        for(GIS_Map_Layer__mdt layer:[select Label,index__c, Layer_URL__c, DeveloperName,Service_Name__c,Service_Site__c,Is_Active__c 
                                      from GIS_Map_Layer__mdt where Is_Active__c=true order by index__c]){
            layerSettingMap.put(layer.DeveloperName, layer);
        }
        return layerSettingMap;
    }
    
    
    @AuraEnabled(cacheable=false)
    public static mapViewDataWrapper getLocations() {
        List<Location> locations = new List<Location>();
        
        for(GIS_Location__c loc:[select id,name,Account__c,Account__r.Name,Account__r.billingCity,Account__r.billingStreet,Location__latitude__s,Location__longitude__s 
                                 from GIS_Location__c where Location__latitude__s!=null and Location__longitude__s!=null and Account__c!=null ]){
            
        Location location = new Location();
        location.name = loc.Account__r.Name;
        location.latitude = loc.Location__latitude__s;
        location.longitude =loc.Location__longitude__s;
        location.locationId = loc.Account__c;
        location.city=loc.Account__r.billingCity!=null?loc.Account__r.billingCity:'';
		location.street=loc.Account__r.billingStreet!=null?loc.Account__r.billingStreet:'';        
                                     
        locations.add(location);
            
            
            
        }
        
        
        map<string,GIS_Map_Layer__mdt> layerSettingMap=getLayerSettings();
        return new mapViewDataWrapper(locations,layerSettingMap);
    }
    public class mapViewDataWrapper{
        @AuraEnabled public List<Location> locations;
        @AuraEnabled public map<string,GIS_Map_Layer__mdt> layerSettings;
        mapViewDataWrapper(List<Location> locations, map<string,GIS_Map_Layer__mdt> layerSettings){
            this.layerSettings =layerSettings;
            this.locations=locations;
        }
        
    }

    public class Location{
        @AuraEnabled public String name;
        @AuraEnabled public String street;
        @AuraEnabled public String city;
        @AuraEnabled public Double latitude;
        @AuraEnabled public Double longitude;
        @AuraEnabled public Id locationId;
    }
}