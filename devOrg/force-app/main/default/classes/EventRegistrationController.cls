public with sharing class EventRegistrationController {
    @AuraEnabled(cacheable=true)
    public static list<Public_Event__c> getEvents() {

        list<Public_Event__c> pubevents = [select id, name, event_start_date__c, event_start_time__c, event_end_date__c, event_end_time__c, registration_start_date__c, registration_end_date__c, maximum_capacity__c, event_status__c, available_seats__c, event_description__c, venue__c, venue_address__c, venue_address_2__c, Venue_City__c,
       Venue_State__c, Venue_Postal_Code__c, waitlisting_capacity__c, available_waitlist_seats__c from public_event__c where event_status__c='Scheduled'];

        //list<Public_Event__c> pubevents = [select id, name from public_event__c where event_status__c='Scheduled'];

        return pubevents;

    }



    @AuraEnabled
    public static string updateRegisteredEvent(string registrationId, string emailid){
        system.debug('registration ID is'+registrationId);
        system.debug('email ID is'+emailid);
        List<event_registration__c> recordForUpdate = new List<event_registration__c>();
        string s;
          recordForUpdate = [select id, first_name__c, last_name__c, confirmed_seats__c, waitlist_seats__c, registration_status__c from event_registration__c
                                         where registration_id__c=:registrationId AND email__c=:emailid];
                                         
             for (event_registration__c er : recordForUpdate) {

                 er.registration_status__c = 'Canceled';
                 er.confirmed_seats__c = 0;
                 er.waitlist_seats__c = 0;
             }

             if(recordForUpdate.size()>0){
                 try{
                    update recordForUpdate;
                    s = 'success';
                 }
                 catch (DMLexception e){
                    s = e.getMessage();
                }
             }
             return s;

             /* return [select id, first_name__c, last_name__c, number_of_seats_booked__c, seats_booked__c from event_registration__c
              where registration_id__c = registrationId
              AND email__c = email LIMIT 1];*/
          }
}