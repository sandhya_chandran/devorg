public class SMSController {
   
    
    public PageReference init(){
        
        Id recordId = ApexPages.currentPage().getParameters().get('RecordId');
        System.debug('===SMSController===' + recordId);
        if(recordId != null){
           String sObjectName = recordId.getSobjectType().getDescribe().getName();
            if(sObjectName == 'Contact'){
                System.debug('===redirect to contact sms===');
                return new PageReference('/apex/TwilioSF__TwoWayMessagesContact?id=' + recordId);
            }else if(sObjectName == 'Lead'){
                return new PageReference('/apex/TwilioSF__TwoWayMessagesLead?id=' + recordId);
            }
        } 
        
        return new PageReference('');
    }
}