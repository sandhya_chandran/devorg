public class Back2WorkController {
    //Set the three font colors to be used for the workspace object (as a class to be used)
    static final String RED_COLOR = 'RED_COLOR';
    static final String GREEN_COLOR = 'GREEN_COLOR';
    static final String GRAY_COLOR = 'GRAY_COLOR';
    static final String TEXT_SIZE  = 'slds-text-heading_medium';
    
    @AuraEnabled(cacheable=true)
    public static List<CSA_Office_Building__c> getOfficeBuildings(){
        return [SELECT Id, Name FROM CSA_Office_Building__c WHERE CSA_Operational__c=TRUE ORDER BY Name ASC];
    }
    
    public class availableFloorsAndWorkspaces{
        List<CSA_Floor__c> availableFloors;
        Map<Id,List<CSA_Workspace__c>> mapOfAvailableOfWorkspaces;
        //Type 0 - Quit because User already has appointment at this time, 
        //Type 1 - No Floors Available, 
        //Type 2 - Normal
        Integer returnType;
    }
    
    private static string getResourceURL(string resourceName){
        List<StaticResource> resourceList= [SELECT Name, NamespacePrefix, SystemModStamp FROM StaticResource WHERE Name = :resourceName Limit 1];
        // Checking if the result is returned or not
        if(resourceList.size() == 1){
            // Getting namespace
            String namespace = resourceList[0].NamespacePrefix;
            // Resource URL
            return '/resource/' + resourceList[0].SystemModStamp.getTime() + '/' + (namespace != null && namespace != '' ? namespace + '__' : '') + resourceName; 
        }
        else return '';
    }
    
    @AuraEnabled(cacheable=true)
    public static String getavailableFloorsAndWorkspaces(String officeId,String contactId, Integer dYear, Integer dMonth, Integer dDay, Integer sHour, Integer sMinute, Integer eHour, Integer eMinute){    
        availableFloorsAndWorkspaces aFAW = New availableFloorsAndWorkspaces();
        DateTime startDate = Datetime.newInstance(dYear, dMonth, dDay, sHour, sMinute, 0);
        DateTime endDate = Datetime.newInstance(dYear, dMonth, dDay, eHour, eMinute, 0);
        //Code section for seeing if the current user already has a booking in this time period (only if an Id is given)
        if (String.isNotEmpty(contactId)){
            List<CSA_Booking__c> b_user = [SELECT Id, CSA_Contact__c, CSA_Workspace__c FROM CSA_Booking__c
                                        WHERE ((CSA_Booking_Start_Time__c >= :startDate AND CSA_Booking_End_Time__c  <= :endDate)
                                        OR (CSA_Booking_Start_Time__c >= :startDate AND CSA_Booking_Start_Time__c <= :endDate AND CSA_Booking_End_Time__c  >= :endDate)
                                        OR(CSA_Booking_Start_Time__c <= :startDate AND CSA_Booking_End_Time__c  >= :endDate)
                                        OR (CSA_Booking_Start_Time__c <= :startDate AND CSA_Booking_End_Time__c  >= :startDate AND CSA_Booking_End_Time__c  <= :endDate)) AND (CSA_Contact__c=:contactId)];
            if (b_user.size()>0){ //Means the user already has an active appointment for this time period
                aFAW.returnType = 0;
                return JSON.serialize(aFAW);
            }
        }           
        //Get the static resources we will need later on
        String desk_url = getResourceURL('desk');
        String desk_gray_url = getResourceURL('desk_gray');
        String desk_red_url = getResourceURL('desk_red');
        
        //Get the id of all the floors building to the Office Building chosen
        Set<Id> fId = new Set<Id>();
        For (CSA_Floor__c f : [Select Id from CSA_Floor__c where CSA_Office_Building__c=:officeId AND CSA_Operational__c=TRUE]){ fId.add(f.Id); }
        
        //From those floors, get all the workspaces (where the workspace is a desk)
        List<CSA_Workspace__c>ws = [Select Id, CSA_Workspace_Name__c, CSA_Operational__c, CSA_Available__c, 
                                    CSA_Floor__c, CSA_textColor__c, CSA_imageUrl__c
                                    From CSA_Workspace__c Where CSA_Floor__c in :fId AND CSA_Type__c='Desk' Order By CSA_Workspace_Name__c ASC];
        
        //Code section for figuring out which bookings overlap with the current time period
        List<CSA_Booking__c> books = [SELECT Id, CSA_Contact__c, CSA_Workspace__c FROM CSA_Booking__c
                                      WHERE (CSA_Booking_Start_Time__c >= :startDate AND CSA_Booking_End_Time__c  <= :endDate)
                                      OR (CSA_Booking_Start_Time__c >= :startDate AND CSA_Booking_Start_Time__c <= :endDate AND CSA_Booking_End_Time__c  >= :endDate)
                                      OR(CSA_Booking_Start_Time__c <= :startDate AND CSA_Booking_End_Time__c  >= :endDate)
                                      OR (CSA_Booking_Start_Time__c <= :startDate AND CSA_Booking_End_Time__c  >= :startDate AND CSA_Booking_End_Time__c  <= :endDate)];
        Set<Id> bookedAppointments = new Set<Id>();
        for (CSA_Booking__c boo : books){ bookedAppointments.add(boo.CSA_Workspace__c); }
        //Now we need to exclude those floors which have no usable desks (all are occupied)
        Map<Id,List<CSA_Workspace__c>> fAW = new Map<Id,List<CSA_Workspace__c>>();
        For (Id i : fId) { fAW.put(i,New List<CSA_Workspace__c>()); }
        For(CSA_Workspace__c w : ws){
            w.CSA_Available__c = w.CSA_Operational__c;
            //If the workspace id is in our bookedAppointments mapped then that means that it is no longer available
            If(bookedAppointments.contains(w.Id)) {w.CSA_Available__c = false; }
            w.CSA_textColor__c = (w.CSA_Available__c?GREEN_COLOR:(w.CSA_Operational__c?RED_COLOR:GRAY_COLOR)) + ' ' + TEXT_SIZE;
            w.CSA_imageUrl__c =  w.CSA_Available__c?desk_url:(w.CSA_Operational__c?desk_red_url:desk_gray_url);
            fAW.get(w.CSA_Floor__c).add(w);
        }
        For (Id ky : fAW.keyset()){
            Boolean canBook = false;
            For (CSA_Workspace__c w : fAW.get(ky)){ canBook = canBook||w.CSA_Available__c; }
            if (!canBook) { fAW.remove(ky); }
        }
        
        //If there are no entries left because all of them were removed then we have no need to go further
        if (fAW.isEmpty()){
            aFAW.returnType = 1;
            return JSON.serialize(aFAW);
        }
        
        //Finally we get the updated list of floors and add them to our return object
        fId = fAW.keyset();
        List<CSA_Floor__c> fs = [Select Id, CSA_Floor_Number__c from CSA_Floor__c where Id in :fId];
        aFAW.availableFloors = fs;
        aFAW.mapOfAvailableOfWorkspaces = fAW;
        aFAW.returnType = 2;
        return JSON.serialize(aFAW);       
    }
    
    @AuraEnabled
    public static Boolean createBooking(String contactId, String workspaceId, 
                                     Integer dYear, Integer dMonth, Integer dDay,
                                     Integer sHour, Integer sMinute, Integer eHour, Integer eMinute){
        CSA_Booking__c b = new CSA_Booking__c();
        b.CSA_Contact__c = contactId;
        b.CSA_Workspace__c = workspaceId;
        b.CSA_Booking_Start_Time__c = Datetime.newInstance(dYear, dMonth, dDay, sHour, sMinute, 0);
        b.CSA_Booking_End_Time__c = Datetime.newInstance(dYear, dMonth, dDay, eHour, eMinute, 0);
        Boolean isGood = True;
        
        if (Schema.sObjectType.CSA_Booking__c.fields.CSA_Contact__c.isCreateable() && Schema.sObjectType.CSA_Booking__c.fields.CSA_Workspace__c.isCreateable() && Schema.sObjectType.CSA_Booking__c.fields.CSA_Booking_Start_Time__c.isCreateable() && Schema.sObjectType.CSA_Booking__c.fields.CSA_Booking_End_Time__c.isCreateable()) {  //Check field level create permission
            Database.SaveResult sr = Database.insert(b);
            if (!sr.isSuccess()) {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('CSA_Booking__c fields that affected this error: ' + err.getFields());
                }
                isGood = false;
            }
        }
        
        return isGood;
    }
}