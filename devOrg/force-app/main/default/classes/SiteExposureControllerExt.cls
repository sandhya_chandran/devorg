public with sharing class SiteExposureControllerExt {


    private final Site_Exposures__c siteExposure;
    
    // The extension constructor initializes the private member
    // variable acct by using the getRecord method from the standard
    // controller.
    public SiteExposureControllerExt(ApexPages.StandardController stdController) {
        this.siteExposure = (Site_Exposures__c)stdController.getRecord();
    }

    public String getSiteExposureJSON() {
        
        system.debug('SiteExposureControllerExt.getSiteExposureJSON() ... START');
        system.debug('-> siteExposure.id=' + siteExposure.id);
        
        SiteExpsoureTreeNode treeNode = new SiteExpsoureTreeNode();
        List<SiteExpsoureTreeNode.ExposedContact> exposedContactList = new List<SiteExpsoureTreeNode.ExposedContact>();
        
        // Query the data
        Site_Exposures__c siteExposureQueried = [SELECT Id, Name, City__c, Location_Name__c
                                                FROM Site_Exposures__c
                                                 WHERE Id=:siteExposure.id];
        system.debug('-> siteExposureQueried=' + siteExposureQueried);
        
        treeNode.Id = String.valueOf(siteExposureQueried.Id);
        treeNode.Name = String.valueOf(siteExposureQueried.Location_Name__c);
        treeNode.value = 1;
        
        // Query the contacts
        List<Site_Exposure_Contact__c> exposedContacts = [SELECT Id, Name, Contact__c, Contact_Name__c, Test_Result__c, Site_Exposure_Name__c
                                                          FROM Site_Exposure_Contact__c
                                                          WHERE Site_Exposure__c =:siteExposure.id];

        Set<id> exposedContactIdSet = new Set<Id>();
        Map<id, List<SiteExpsoureTreeNode.ExposedIndividual> > exposedContactMap = new Map<id, List<SiteExpsoureTreeNode.ExposedIndividual> >();
        for (Site_Exposure_Contact__c sec: exposedContacts)
        {
            SiteExpsoureTreeNode.ExposedContact ec = new SiteExpsoureTreeNode.ExposedContact();
            
            ec.id = sec.Contact__c;
            ec.Name = sec.Contact_Name__c;
            ec.value = 1;
            exposedContactList.add(ec);
            
            exposedContactIdSet.add(sec.Contact__c);
        }
        
        // Query the leads
        List<Lead> exposedIndividuals = [SELECT id, Name, Contact__c
                                         FROM Lead
                                         WHERE Contact__c IN: exposedContactIdSet];
        
        for (Lead ei: exposedIndividuals)
        {
            List<SiteExpsoureTreeNode.ExposedIndividual> exposedInvList = null;
            if ( exposedContactMap.get(ei.Contact__c) == null )
            {
                exposedInvList = new List<SiteExpsoureTreeNode.ExposedIndividual>();
            }
            else
            {
                exposedInvList = exposedContactMap.get(ei.Contact__c);
            }

            SiteExpsoureTreeNode.ExposedIndividual ev = new SiteExpsoureTreeNode.ExposedIndividual();
            ev.id = ei.Id;
            ev.Name = ei.Name;
            ev.value = 1;
            exposedInvList.add(ev);
            
            // Update map
            exposedContactMap.put(ei.Contact__c, exposedInvList);
        }        
        
        // Populate the list of Contact children
        for ( SiteExpsoureTreeNode.ExposedContact ec: exposedContactList)
        {
            List<SiteExpsoureTreeNode.ExposedIndividual> eiList = exposedContactMap.get(ec.Id);
            
            if ( eiList != null )
            {
                ec.children = eiList;
            }
        }
        
        treeNode.children = exposedContactList;
        
        String jsonStr = SiteExpsoureTreeNode.convertToJSON(treeNode);
        
        system.debug('-> jsonStr=' + jsonStr);
        
        return jsonStr;
    }
}