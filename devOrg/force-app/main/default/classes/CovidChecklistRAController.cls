global class CovidChecklistRAController extends VlocityRemoteActionBase {
  global override void execute() {
    System.debug('== CovidChecklistRAController ==');

    if (methodName == 'initVisitorOSDataSeed') {
      initVisitorOSDataSeed();
    } else if (methodName == 'validateConfirmCode') {
      validateConfirmCode();
    } else if (methodName == 'saveVisitorChecklist') {
      saveVisitorChecklist();
    }
  }

  global void saveVisitorChecklist() {
    //get visitor recordtype id
    Id visitorRecordTypeId = Schema.SObjectType.Prevention_Checklist__c.getRecordTypeInfosByName()
      .get('Visitor')
      .getRecordTypeId();
    System.debug(visitorRecordTypeId);
    Map<String, Object> contactInfoMap = getMap('step_visitor_info');
    String email = get('text_visitor_email', contactInfoMap);
    String contactName = get('text_visitor_name', contactInfoMap);
    String phone = get('text_visitor_phone', contactInfoMap);

    System.debug('===email===' + email);
    System.debug('===contactName===' + contactName);
    System.debug('===phone===' + phone);

    Confirmation_Code__c cc = new Confirmation_Code__c();
    cc.Active__c = true;
    cc.Email__c = email;
    cc.Phone__c = phone;
    cc.Contact_Name__c = contactName;
    if(Schema.sObjectType.Confirmation_Code__c.fields.Active__c.isCreateable() && Schema.sObjectType.Confirmation_Code__c.fields.Email__c.isCreateable() && Schema.sObjectType.Confirmation_Code__c.fields.Phone__c.isCreateable() && Schema.sObjectType.Confirmation_Code__c.fields.Contact_Name__c.isCreateable()){ //check FLS for creation
        insert cc;
    }
    
    Map<String, Object> stepPurposeOfVisit = getMap('step_purpose_of_visit');

    String purposeOfVisit = get(
      'text_What_is_the_purpose_of_your_visit',
      stepPurposeOfVisit
    );

    Confirmation_Code__c insertedCC = [
      SELECT name
      FROM Confirmation_Code__c
      WHERE Id = :cc.Id
    ];

    Prevention_Checklist__c pc = new Prevention_Checklist__c();
    pc.Confirmation_Code__c = cc.id;
    pc.purpose_of_visit__c = purposeOfVisit;
    if(Schema.sObjectType.Prevention_Checklist__c.fields.Confirmation_Code__c.isCreateable() && Schema.sObjectType.Prevention_Checklist__c.fields.purpose_of_visit__c.isCreateable()){ //check FLS for creation
        insert pc;
    }
    put('newConfirmCode', insertedCC.Name);
  }

  global void initVisitorOSDataSeed() {
    System.debug('== initVisitorOSDataSeed ===');
  }

  global void validateConfirmCode() {
    Map<String, Object> stepConfirmCodeMap = getMap('step_confirm_code');

    String code = get('text_confirm_code', stepConfirmCodeMap);
    System.debug('=== confirm code === ' + code);
    Boolean isValidConfirmCode = false;
    if (String.isNotBlank(code)) {
      List<Confirmation_Code__c> confirms = [
        SELECT id
        FROM Confirmation_Code__c
        WHERE name = :code AND active__c = true AND Code_Expired__c = false
        LIMIT 1
      ];
      System.debug('==== confirms ===' + confirms);
      if (confirms.size() > 0) {
        put('isValidConfirmCode', 'true');
      } else {
        throw new VlocityRemoteActionException(
          'Confirmation code: ' +
          code +
          ' is invalid or expired.'
        );
      }
    } else {
      throw new VlocityRemoteActionException(
        'Confirmation code cannot be empty'
      );
    }
  }
}