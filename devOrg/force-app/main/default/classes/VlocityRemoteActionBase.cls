//Base Calss for Vloocity Remote Action
//1. Handle Exception, Throw error message to the end user
//2. Rollback DB changes when exception occurs
//3. Generic exception
//4. Generic util functions
//Init Build: 2019-02-02 Zeeman Chen
//
global virtual class VlocityRemoteActionBase implements vlocity_ps.VlocityOpenInterface {
    protected Map<String, Object> inputMap {get;set;}
    protected Map<String, Object> outMap {get;set;}
    protected Map<String, Object> optionMap {get;set;}
    protected String methodName {get;set;}
    protected Boolean success {get;set;}
    
    global Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> optionMap) {
        success = true;
        this.inputMap = inputMap;
        this.outMap = outMap;
        this.optionMap = optionMap;
        this.methodName = methodName;
        
        System.debug('====' + methodName +'====');
        System.debug('==inputMap==' + inputMap);
        System.debug('==outMap==' + outMap);
        System.debug('==optionMap==' + optionMap);
        
        SavePoint sp = Database.setSavepoint();
        
        try{
            execute();
        }catch(Exception ex){
            outMap.put('error', ex.getMessage());
            success = false;
            Database.rollback(sp);
        }
        
        return success;
    }
    
    protected virtual void execute(){}
    
    @TestVisible
    protected void put(String key, String value){
        outMap.put(key, value);
    }
    
    @TestVisible
    protected String getString(String key){
        return inputMap.get(key) == null ? null : (String)inputMap.get(key);
    }
    
    @TestVisible
    protected String get(String key){
        return getString(key);
    }
    
    @TestVisible
    protected String getString(String key, Map<String, Object> m){
        return m.get(key)== null ? null: (String)m.get(key);
    }
    
    @TestVisible
    protected String get(String key, Map<String, Object> m){
        return getString(key,m);
    }
    
    
    
    @TestVisible
    protected Integer getInt(String key){
        return inputMap.get(key) == null ? null : (Integer)inputMap.get(key);
    }
    
    @TestVisible
    protected Boolean getBoolean(String key){
        return inputMap.get(key) == null ? false : (Boolean)inputMap.get(key);
    }
    
    @TestVisible
    protected Boolean getBoolean(String key, Map<String, Object> m){
        return m.get(key) == null? false : (Boolean)m.get(key);
    }
    
    @TestVisible
    protected Map<String, Object> getMap(String mapName){        
        return inputMap.get(mapName) == null ? new Map<String, Object>() : (Map<String, Object>)inputMap.get(mapName);
    }
    
    @TestVisible
    protected List<Object> getList(String key){
        List<Object> objList;
        Object obj = inputMap.get(key);
        if(obj instanceof List<Object>){
            return (List<Object>)obj;
        }else{
            objList = new List<Object>();
            if(obj != null){
                objList.add(obj);
            }
        }
        
        return objList;
    }
    
    @TestVisible
    protected List<Object> getList(String key, Map<String,Object> m){
        List<Object> objList;
        Object obj = m.get(key);
        if(obj instanceof List<Object>){
            return (List<Object>)obj;
        }else{
            objList = new List<Object>();
            if(obj != null){
                objList.add(obj);
            }
        }
        
        return objList;
    }
    
    public class VlocityRemoteActionException extends Exception {}
}