public class MonthlyIncomeController {
    public List<IncomeData> getIncomeData() {
        List<IncomeData> data = new List<IncomeData>();
        data.add(new IncomeData('Jan 2016', 1400));
        data.add(new IncomeData('Feb 2016', 1400));
        data.add(new IncomeData('Mar 2016', 2100));
        data.add(new IncomeData('Apr 2016', 1400));
        data.add(new IncomeData('May 2016', 1400));
        data.add(new IncomeData('June 2016', 1400));
        data.add(new IncomeData('July 2016', 1400));
        data.add(new IncomeData('Aug 2016', 1400));
        data.add(new IncomeData('Sep 2016', 1400));
        data.add(new IncomeData('Oct 2016', 1400));
        data.add(new IncomeData('Nov 2016', 1400));
        data.add(new IncomeData('Dec 2016', 1400));
        return data;
    }

    // Wrapper class
    public class IncomeData {

        public String status { get; set; }
        public Integer points { get; set; }

        public IncomeData(String status, Integer points) {
            this.status = status;
            this.points = points;
        }
    }
}