public class ZipFileController {
    @auraEnabled
    public static FileDto getFile(String fileId){
       System.debug('====ZipFileController.getFile===='+ fileId);
       ContentVersion file = 
           [SELECT Id, FileType, FileExtension,
            ContentSize, VersionData, 
            Title, ContentDocumentId, 
            IsLatest FROM ContentVersion
            WHERE ContentDocumentId =: fileId
            AND IsLatest = true];
        System.debug('===file===' + file);
        
        FileDto dto = new FileDto();
        dto.fileName = file.Title;
        dto.content = EncodingUtil.base64Encode(file.VersionData);
        dto.FileExtension = file.FileExtension;
        return dto;
            
    }
    
    public class FileDto {
        @auraEnabled
        public String fileName {get;set;}
        @auraEnabled
        public String content {get;set;}
        @auraEnabled
        public String fileExtension {get;set;}
        
    }
}