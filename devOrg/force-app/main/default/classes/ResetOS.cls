global class ResetOS implements vlocity_ps.VlocityOpenInterface{

    global Boolean invokeMethod(String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){

        system.debug('In formula methodName: ' + methodName);
        system.debug(':::inputs: ' + input);
        List<Object> arguments = (List<Object>) input.get('arguments');
  
        
        if(methodName == 'resetDemo'){
            return resetDemo();
        }
        
        return false;
    }

  private Boolean resetDemo() {
  Contact[] deleteContacts = [SELECT Id, Name FROM Contact WHERE LastName = 'Ruiz' OR LastName = 'Diaz' OR LastName = 'Saunders'];
 
    try {
         if (Schema.sObjectType.Contact.isDeletable()) { //Check object-level permission for obj before deleting 
            delete deleteContacts;
         }
    } catch (DmlException e) {
    // Process exception here
    }

  vlocity_ps__Household__c[] deleteHouseholds = [SELECT Id, Name FROM vlocity_ps__Household__c WHERE Name = 'Ruiz Household'];
 
    try {
        if (Schema.sObjectType.vlocity_ps__Household__c.isDeletable()) { //Check object-level permission for obj before deleting 
            delete deleteHouseholds;
        }
    } catch (DmlException e) {
    // Process exception here
    }

  vlocity_ps__ProgramEnrollment__c[] deletePEs = [SELECT Id, Name FROM vlocity_ps__ProgramEnrollment__c WHERE Name = 'Lisa Saunders - Housing'];
 
    try {
        if (Schema.sObjectType.vlocity_ps__ProgramEnrollment__c.isDeletable()) { //Check object-level permission for obj before deleting
            delete deletePEs;
        }
    } catch (DmlException e) {
    // Process exception here
    }

    return true;

  }
}